Gets current weather conditions from the National Weather Service.

NWSWeather has been tested with Limnoria 2016.05.06 on Python 2.7.9 and Limnoria 2017.01.11 on Python 3.4.2.
