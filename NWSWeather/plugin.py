###
# Copyright (c) 2017, xoddf2
# All rights reserved.
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
# OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#
# Possible ideas for additional features:
# - Alerts
# - Reporting-site registration tied to Supybot account
#
# TODO:
# - Reduce redundancy (DRY).
###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('NWSWeather')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x

import sys
if sys.version_info[0] >= 3:
    import urllib.request
else:
    import urllib
import urllib
import re
import cmath

if sys.version_info[0] >= 3:
    def get_conditions(url):
        return str(urllib.request.urlopen(url).read())
else:
    def get_conditions(url):
        return str(urllib.urlopen(url).read())

if sys.version_info[0] >= 3:
    def u(s):
        return s
else:
    def u(s):
        return unicode(s, 'unicode_escape')


class NWSWeather(callbacks.Plugin):
    """Gets current weather conditions from the National Weather Service."""
    threaded = True

    def weather(self, irc, msg, args, airport):
        """<ICAO identifier>

        Returns the current temperature and sky condition at a given reporting
        site from the National Weather Service.
        """
        try:
            # Get the current conditions
            conditions = get_conditions(
                'http://tgftp.nws.noaa.gov/data/observations/metar/decoded/'
                + airport.upper() + '.TXT')

            # Get the METAR
            metar = get_conditions(
                'http://tgftp.nws.noaa.gov/data/observations/metar/stations/'
                + airport.upper() + '.TXT')
            # Remove the date and time, leaving just the METAR
            metar = str(re.sub('\\\\n\'$', '', re.findall(airport + '.*$',
                                                          metar)[0]))

            # Name of reporting site
            try:
                siteName = re.sub('^b[\'"]', '',
                                  str(re.findall('^.*?(?= ?,)',
                                                 conditions.split('\\n',
                                                                  1)[0])[0]))
            except:
                siteName = airport.upper()

            # Sky condition
            if sys.version_info[0] >= 3:
                if re.match('.*\\\\nWeather: .*', conditions):
                    sky = re.findall('(?<=Weather: ).*?(?=\\\\n)',
                                     conditions)[0]
                elif re.findall('NCD', str(metar)):
                    sky = 'no cloud detected'
                elif re.findall('NSC', str(metar)):
                    sky = 'no significant cloud'
                else:
                    sky = re.findall('(?<=Sky conditions: )[a-z ]*(?=\\\\n.*)',
                                     conditions)[0]
            else:
                if re.match('.*\nWeather: .*', conditions):
                    sky = re.findall('(?<=Weather: ).*?(?=\n)',
                                     conditions)[0]
                elif re.findall('NCD', str(metar)):
                    sky = 'no cloud detected'
                elif re.findall('NSC', str(metar)):
                    sky = 'no significant cloud'
                else:
                    sky = re.findall('(?<=Sky conditions: )[a-z ]*(?=\n.*)',
                                     conditions)[0]

            # Temperature
            tempC = re.sub('M', '-', str(re.findall('M?[0-9]*(?=/)',
                                                    metar)[0]))
            tempF = int(tempC) * 9 / 5 + 32

            output = []

            output.append(siteName + ': ')
            output.append(str(int(tempC)) + u'\u00b0C/' + str(int(tempF)))
            output.append(u'\u00b0F ' + sky)

            irc.reply(self._formatUnicodeOutput(output))
        except utils.web.Error:
            irc.reply(str('The requested location was not found.  Please ' +
                          'enter an ICAO airport code.'))

    weather = wrap(weather, ['text'])

    def metar(self, irc, msg, args, airport):
        """<ICAO identifier>

        Returns the raw METAR for a given reporting site.
        """
        try:
            # Get the METAR
            metar = get_conditions(
                'http://tgftp.nws.noaa.gov/data/observations/metar/stations/'
                + airport.upper() + '.TXT')
            # Remove the date and time, leaving just the METAR
            irc.reply(str(re.sub('\\\\n\'$', '', re.findall(airport + '.*$',
                                                            metar)[0])))
        except utils.web.Error:
            irc.reply(str('The requested location was not found.  Please ' +
                          'enter an ICAO airport code.'))

    metar = wrap(metar, ['text'])

    def current(self, irc, msg, args, options, airport):
        """[--metric] <ICAO identifier>

        Returns detailed current conditions at a given reporting site from the
        National Weather Service.  Due to a difference in METAR formats, only
        locations in North America are supported.
        """
        metric = False

        for (type, arg) in options:
            if type == 'metric':
                metric = True

        try:
            # Get the current conditions
            conditions = get_conditions(
                'http://tgftp.nws.noaa.gov/data/observations/metar/decoded/'
                + airport.upper() + '.TXT')

            # Get the METAR
            metar = get_conditions(
                'http://tgftp.nws.noaa.gov/data/observations/metar/stations/'
                + airport.upper() + '.TXT')
            # Remove the date and time, leaving just the METAR
            metar = str(re.sub('\\\\n\'$', '', re.findall(airport + '.*$',
                                                          metar)[0]))

            # Name of reporting site
            try:
                siteName = re.sub('^b[\'"]', '',
                                  str(re.findall('^.*?(?= ?,)',
                                                 conditions.split('\\n',
                                                                  1)[0])[0]))
            except:
                siteName = airport.upper()

            # Sky condition
            if sys.version_info[0] >= 3:
                if re.match('.*\\\\nWeather: .*', conditions):
                    sky = re.findall('(?<=Weather: ).*?(?=\\\\n)',
                                     conditions)[0]
                elif re.findall('NCD', str(metar)):
                    sky = 'no cloud detected'
                elif re.findall('NSC', str(metar)):
                    sky = 'no significant cloud'
                else:
                    sky = re.findall('(?<=Sky conditions: )[a-z ]*(?=\\\\n.*)',
                                     conditions)[0]
            else:
                if re.match('.*\nWeather: .*', conditions):
                    sky = re.findall('(?<=Weather: ).*?(?=\n)',
                                     conditions)[0]
                elif re.findall('NCD', str(metar)):
                    sky = 'no cloud detected'
                elif re.findall('NSC', str(metar)):
                    sky = 'no significant cloud'
                else:
                    sky = re.findall('(?<=Sky conditions: )[a-z ]*(?=\n.*)',
                                     conditions)[0]

            # Temperature
            tempC = re.sub('M', '-', str(re.findall('M?[0-9]*(?=/)',
                                                    metar)[0]))
            tempF = int(tempC) * 9 / 5 + 32

            # Wind
            windSpeed = re.findall('(?<= [0-9V][0-9R][0-9B])[0-9]*(?=[GK])',
                                   metar)[0]
            windGusts = re.findall('(?<=[0-9]G)[0-9]*(?=KT)', metar)
            windDirectionDegs = re.sub('^0+', '', re.findall(
                '(?<= )[0-9V][0-9R][0-9B](?=[0-9G]*KT)', metar)[0])

            # Convert wind direction from azimuth degs to cardinal directions
            try:
                if int(windDirectionDegs) in range(0, 10):
                    windDirection = 'N'
                elif int(windDirectionDegs) in range(11, 34):
                    windDirection = 'NNE'
                elif int(windDirectionDegs) in range(35, 56):
                    windDirection = 'NE'
                elif int(windDirectionDegs) in range(57, 79):
                    windDirection = 'ENE'
                elif int(windDirectionDegs) in range(80, 101):
                    windDirection = 'E'
                elif int(windDirectionDegs) in range(102, 124):
                    windDirection = 'ESE'
                elif int(windDirectionDegs) in range(125, 146):
                    windDirection = 'SE'
                elif int(windDirectionDegs) in range(147, 169):
                    windDirection = 'SSE'
                elif int(windDirectionDegs) in range(170, 191):
                    windDirection = 'S'
                elif int(windDirectionDegs) in range(192, 214):
                    windDirection = 'SSW'
                elif int(windDirectionDegs) in range(215, 236):
                    windDirection = 'SW'
                elif int(windDirectionDegs) in range(237, 259):
                    windDirection = 'WSW'
                elif int(windDirectionDegs) in range(260, 281):
                    windDirection = 'W'
                elif int(windDirectionDegs) in range(282, 304):
                    windDirection = 'WNW'
                elif int(windDirectionDegs) in range(305, 326):
                    windDirection = 'NW'
                elif int(windDirectionDegs) in range(327, 360):
                    windDirection = 'N'
                else:
                    windDirection = windDirectionDegs + 'deg'
            except ValueError:
                pass

            if windSpeed == '00':
                wind = 'Wind: Calm'
                windKph = 'Wind: Calm'
            elif windGusts:
                wind = 'Wind: ' + windDirection + ' ' + \
                       re.sub('^0*', '', str(int(int(windSpeed) * 1.15)) +
                              ' MPH | Gusts: ' +
                              str(int(int(windGusts[0]) * 1.15))) + ' MPH'
                windKph = 'Wind: ' + windDirection + ' ' + \
                          re.sub('^0*', '', str(int(int(windSpeed) * 1.852)) +
                                 ' km/h | Gusts: ' +
                                 str(int(int(windGusts[0]) * 1.852))) + ' km/h'
            else:
                wind = 'Wind: ' + windDirection + ' ' + \
                       str(int(int(windSpeed) * 1.15)) + ' MPH'
                windKph = 'Wind: ' + windDirection + ' ' + \
                          str(int(int(windSpeed) * 1.852)) + ' km/h'

            # Dew Point
            dewPointC = re.sub('M', '-', re.findall('(?<=/)M*[0-9]*(?= )',
                                                    metar)[0])
            dewPointF = int(int(dewPointC) * 9 / 5 + 32)

            # Relative Humidity
            es = cmath.exp((17.269 * float(tempC)) / (237.3 + float(tempC)))
            e = cmath.exp((17.269 * float(dewPointC)) / (237.3 +
                                                         float(dewPointC)))
            humidity = int(abs(e * 100 / es))

            # Barometric Pressure
            pressure = re.findall('(?<=A)[0-9][0-9][0-9][0-9]',
                                  metar)[0]
            pressureFormatted = pressure[0] + pressure[1] + '.' + \
                pressure[2] + pressure[3]
            pressurehPa = float(pressureFormatted) * 33.86389

            # Visibility
            visibility = re.findall('[0-9]*(?=SM)', metar)[0]
            visibilityKm = str(int(float(visibility) * float(1.6)))

            output = []

            if metric:
                output.append(siteName + ': ')
                output.append(str(int(tempC)) + u'\u00b0C ')
                output.append(sky + ' | ')
                output.append(windKph)
                output.append(' | Humidity: ' + str(humidity) + '% | ')
                output.append('Dew Point: ' + str(int(dewPointC)) + u'\u00b0C')
                output.append(' | Pressure: ' + str(int(pressurehPa)) + ' hPa')
                output.append(' | Visibility: ' + str(visibilityKm) + ' km')
            else:
                output.append(siteName + ': ')
                output.append(str(int(tempF)) + u'\u00b0F ')
                output.append(sky + ' | ')
                output.append(wind)
                output.append(' | Humidity: ' + str(humidity) + '% | ')
                output.append('Dew Point: ' + str(int(dewPointF)) + u'\u00b0F')
                output.append(' | Pressure: ' + str(pressureFormatted))
                output.append(' in. | Visibility: ' + str(visibility) + ' mi.')

            irc.reply(self._formatUnicodeOutput(output))
        except utils.web.Error:
            irc.reply(str('The requested location was not found.  Please ' +
                          'enter an ICAO airport code.'))
        except IndexError:
            irc.reply(str('Only North American locations are supported.'))

    current = wrap(current, [getopts({'metric': ''}), additional('text')])

    def _formatUnicodeOutput(output):
        s = u('').join(output)
        if sys.version_info[0] < 3:
            s = s.encode('utf-8')
        return s
    _formatUnicodeOutput = staticmethod(_formatUnicodeOutput)

Class = NWSWeather


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
