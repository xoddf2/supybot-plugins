###
# Copyright (c) 2017, xoddf2
# All rights reserved.
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
# OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
###

from supybot.test import *


class NWSWeatherTestCase(PluginTestCase):
    plugins = ('NWSWeather',)
    if network:
        def testWeather(self):
            self.assertNotError("weather KJFK")
            self.assertNotError("weather KLAX")
            self.assertNotError("weather KORD")

            # Non-existent
            self.assertError("weather foobar")

            # Capitalisation
            self.assertNotError("weather kphl")
            self.assertNotError("weather Ksfo")
            self.assertNotError("weather KdCa")

            # AK and HI
            self.assertNotError("weather PANC")
            self.assertNotError("weather PHNL")

            # Non-US
            self.assertNotError("weather EGLL")
            self.assertNotError("weather RJTT")
            self.assertNotError("weather YSSY")
            self.assertNotError("weather LFPG")
            self.assertNotError("weather CYYZ")

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
